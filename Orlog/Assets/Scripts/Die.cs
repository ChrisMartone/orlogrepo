﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Die : MonoBehaviour
{
    // Start is called before the first frame update 
    void Start()
    {
        
        
    }

    public DieLockIndicator0 dieLockIndicator0;
    public Faces faces;

    int faceNum;

    bool lockStatus = false;
    bool permaLock = false;

    //Component linked to image
    public Image dieFace;

    public Sprite CurrentFace;
    
    
    public bool[] godFavor;
    public Faces[] dieFaces;

    //TODO: Make a Faces Subclass, Faces Array contains 'Faces' | Struct? 


    /* These are set in inspector
     0. AxeFace
     1. HelmetFace
     2. ArrowFace
     3. ShieldFace
     4. HandFace
     */


    // Update is called once per frame
    void Update()
    {
        dieFace.sprite = CurrentFace;
    }


    public void rollDie()
    {
        //Get a random face - IF the die is not locked
        if (!lockStatus)
        {
            //CurrentFace = faces.
            int randomNum = Random.Range(0, 5);
            CurrentFace = dieFaces[randomNum].getFaceSprite();
            faceNum = randomNum;
        }
    }




    public void ToggleLockStatus()
    {
        //Checks to see if die is now permenetly locked
        if (!permaLock)
        {
            //Switch between locked and unlocked state
            if (lockStatus == false)
            {
                lockStatus = true;
                dieLockIndicator0.ChangeStatusImage(1);
            }
            else
            {
                lockStatus = false;
                dieLockIndicator0.ChangeStatusImage(0);
            }
        }

    }

    public bool GetLockStatus()
    {
        return lockStatus;
    }

    public void SetPermaLock()
    {
        permaLock = true;
    }

    public void RemovePermaLock()
    {
        permaLock = false;
    }


    public bool IsGodFavor()
    {
        return dieFaces[faceNum].IsToken();
    }


}
