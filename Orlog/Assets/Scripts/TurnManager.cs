﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TurnManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    bool playersTurn;
    bool inTurn = false;

    int priority = 0;
    int turnCounter = 1;

    public PlayerDieManager playerDieManager;
    public OpponentDieManager opponentDieManager;
    public PlayerHealthManager playerHealthManager;



    // Update is called once per frame
    void Update()
    {
        
    }


    public void TakeTurn()
    {
        //Determine which player is going
        if (playersTurn & !inTurn)
        {
            playerDieManager.RollDice();
            inTurn = true;
            Debug.Log("Its the players turn");
        }
        else if (!playersTurn & !inTurn)
        {
            opponentDieManager.RollDice();
            inTurn = true;
            Debug.Log("Its the opponents turn");
        }
    }

    //Add Logic that opens the listener for END_GAME()


    public void PassTurn()
    {
        if (playersTurn)
        {
            playersTurn = false;
            inTurn = false;
        }
        else
        {
            playersTurn = true;
            inTurn = false;
        }
    }


    public void RandomPriority()
    {
        priority = Random.Range(0, 2);
    }


    public void SwitchPriority()
    {
        if (priority == 1)
        {
            priority = 0;
        }
        else
        {
            priority = 1;
        }
    }

    public int GetTurnCount()
    {
        return turnCounter;
    }

    public void IncreaseTurnCounter()
    {
        turnCounter += 1;
    }


    public void ResetTurnManager()
    {
        inTurn = false;
        turnCounter = 1;
        SwitchPriority();
    }



}
