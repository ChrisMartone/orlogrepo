﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpponentDieManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
     //   RollDice();
    }

    public Die[] dice;

    // Update is called once per frame
    void Update()
    {

    }

    public void RollDice()
    {
        for (int i = 0; i < 6; i++)
        {
            dice[i].rollDie();
        }
    }

    public void UpdateDieLockStatus(int dieNum)
    {
        dice[dieNum].ToggleLockStatus();
    }
}
