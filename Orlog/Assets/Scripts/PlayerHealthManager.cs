﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealthManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        healthText.text = "Player Health: " + health;
    }

    public int health = 15;
    public Text healthText;


    // Update is called once per frame
    void Update()
    {
        //TESTING
        healthText.text = "Player Health: " + health;
    }


    public int GetPlayerHealth()
    {
        return health;
    }

    public void IncreasePlayerHealth(int increase)
    {
        health += increase;
    }

    public void DecreasePlayerHealth(int decrease)
    {
        health -= decrease;
    }

    public bool IsPlayerDead()
    {
        if (health <= 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}

