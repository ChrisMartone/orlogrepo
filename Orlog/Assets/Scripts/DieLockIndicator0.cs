﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DieLockIndicator0 : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        image.sprite = status[0];
    }

    public Image image;

    public Sprite[] status;

    // Update is called once per frame
    void Update()
    {

    }

    public void ChangeStatusImage(int toggle)
    {
        if(toggle == 1)
        {
            image.sprite = status[1];
            
        }
        else
        {
            image.sprite = status[0];
        }
    }
    
}
