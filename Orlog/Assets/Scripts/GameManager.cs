﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        //Coinflip for who has starting priority
        turnManager.RandomPriority();

    }

    public PlayerDieManager playerDieManager;
    public OpponentDieManager opponentDieManager;
    public PlayerHealthManager playerHealthManager;
    public OpponentHealthManager opponentHealthManager;
    public PlayerGodFavorManager playerGodFavorManager;

    public TurnManager turnManager;
    

    // 0 = Player, 1 = Opponent

    int playersTurn = 1;
    int rollNumber = 1;

    bool gameOver = false;
    bool endTurn = false;



    // Update is called once per frame
    void Update()
    {
        // Check if 6 turns have gone by
        if (turnManager.GetTurnCount() > 6)
        {
            endRound();
            turnManager.ResetTurnManager();
        }



        if (!endTurn)
        {
            //Do Turn
            turnManager.TakeTurn();
        }
        else
        {
            //Pass the turn

            if (turnManager.GetTurnCount() <= 6)
            {
                turnManager.PassTurn();
                turnManager.IncreaseTurnCounter();
                Debug.Log("Turn: " + turnManager.GetTurnCount());
            }

            endTurn = false;
        }



        if (gameOver)
        {
            //End the round 
        }




    }


    void endRound()
    {
        if (healthCheck())
        {
            //Do something to end the game
            Debug.Log("--GAME OVER--");
        }

        Debug.Log("MADE IT TO END ROUND");

        //After all 3 rolls > player chooses God Favor
        choseGodFavor();

        //Resolve Dice Effects

        //Resolve Dice God Tokens

        //Resolve God Favor

        //Switch Priority 

        //Prepares Turn Manager for next round
        turnManager.ResetTurnManager();

    }

    bool healthCheck()
    {
        if ( playerHealthManager.IsPlayerDead() | opponentHealthManager.IsOpponentDead())
        {
            return true;
        }
        else
        {
            return false;
        }
    }


    void choseGodFavor()
    {
        //Some logic that determins what favor is being used


    }

    public void endTurnButton()
    {
        endTurn = true;
    }

}
