﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Faces : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }



    public bool isToken;

    public Sprite face;

    public enum faceType 
    { 
        AXE,
        ARROW,
        HELMET,
        SHIELD,
        HAND
    }


    // Update is called once per frame
    void Update()
    {
        
    }

    public Sprite getFaceSprite()
    {
        return face;
    }

    public bool IsToken()
    {
        return isToken;
    }

}
