﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleGodFavorMenu : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public GameObject godFavorMenu;
    bool isShowing = true;

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ToggleMenuOn()
    {
        godFavorMenu.SetActive(true);
    }

    public void ToggleMenuOff()
    {
        godFavorMenu.SetActive(false);
    }



}
