﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OpponentHealthManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        healthText.text = "Player Health: " + health;
    }

    public int health = 15;
    public Text healthText;


    // Update is called once per frame
    void Update()
    {
        //TESTING
        healthText.text = "Opponent's Health: " + health;
    }


    public int GetOpponentHealth()
    {
        return health;
    }

    public void IncreaseOpponentHealth(int increase)
    {
        health += increase;
    }

    public void DecreaseOpponentHealth(int decrease)
    {
        health -= decrease;
    }

    public bool IsOpponentDead()
    {
        if (health <= 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
