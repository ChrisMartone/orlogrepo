﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerDieManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        //RollDice();
    }

    public Die[] dice;


    // Update is called once per frame
    void Update()
    {
        
    }

    public void RollDice()
    {
        for ( int i = 0; i < 6; i++ )
        {
            dice[i].rollDie();
        }
    }
    
    public void UpdateDieLockStatus(int dieNum)
    {
        dice[dieNum].ToggleLockStatus();
    }

    public Die getDieFace(int faceNum)
    {
        //Return Die Face and God Token Status 
        return dice[faceNum]; 
    }

}
